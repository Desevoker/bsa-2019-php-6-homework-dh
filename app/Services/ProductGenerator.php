<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Product;

class ProductGenerator
{
    /**
     * @return Product[]
     */
    public static function generate(): array
    {
        return collect(self::getProductsData())->map(function ($productData) {
            return new Product(...$productData);
        })->all();
    }

    private static function getProductsData(): array
    {
        return [
            [
                2,
                'product2',
                200.22,
                'url2',
                2.2
            ],
            [
                1,
                'product1',
                100.11,
                'url1',
                1.1
            ],
            [
                3,
                'product3',
                300.33,
                'url3',
                3.3
            ],
            [
                4,
                'product4',
                400.44,
                'url4',
                4.4
            ]
        ];
    }
}
