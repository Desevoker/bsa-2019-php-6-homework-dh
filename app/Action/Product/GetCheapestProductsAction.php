<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsAction
{
    private $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function execute(): GetCheapestProductsResponse
    {
        $products = collect($this->repository->findAll())->sortBy(function ($product) {
            return $product->getPrice();
        })->values()->take(3)->all();

        return new GetCheapestProductsResponse($products);
    }
}
