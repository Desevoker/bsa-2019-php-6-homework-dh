<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetMostPopularProductAction
{
    private $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function execute(): GetMostPopularProductResponse
    {
        $product = collect($this->repository->findAll())->sortByDesc(function ($product) {
            return $product->getRating();
        })->values()->first();

        return new GetMostPopularProductResponse($product);
    }
}
