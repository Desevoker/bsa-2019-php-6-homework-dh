<?php

namespace App\Http\Controllers;

use App\Action\Product\GetCheapestProductsAction;

class ProductController extends Controller
{
    public function getCheapest(GetCheapestProductsAction $action)
    {
        $response = $action->execute();
        $products = $response->getProducts();

        return view('products.cheap', compact('products'));
    }
}
