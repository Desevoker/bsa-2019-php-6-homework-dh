<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Action\Product\GetAllProductsAction;
use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;

class ProductController extends Controller
{
    public function getAll(GetAllProductsAction $action)
    {
        $response = $action->execute();
        $products = $response->getProducts();

        return ProductArrayPresenter::presentCollection($products);
    }

    public function getMostPopular(GetMostPopularProductAction $action)
    {
        $response = $action->execute();
        $product = $response->getProduct();

        return ProductArrayPresenter::present($product);
    }
}
