<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cheap products</title>
</head>
<body>
    <div style="display: flex; flex-direction: row; justify-content: center; align-items: center;">
    @foreach ($products as $product)
        <div style="margin: 4%;">
            <p>Id: {{ $product->getId() }}.</p>
            <p>Name: <strong>{{ $product->getName() }}</strong></p>
            <div><img src="{{ $product->getImageUrl() }}" alt="product image"></div>
            <p>Price: {{ $product->getPrice() }}</p>
            <p>Rating: {{ $product->getRating() }}</p>
        </div>
    @endforeach
    </div>
</body>
</html>
